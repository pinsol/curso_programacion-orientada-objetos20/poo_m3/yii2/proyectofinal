<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property int $id
 * @property string|null $texto
 *
 * @property Preguntascatego[] $preguntascategos
 * @property Preguntas[] $idPreguntas
 * @property Tests[] $tests
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
        ];
    }

    /**
     * Gets query for [[Preguntascategos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntascategos()
    {
        return $this->hasMany(Preguntascatego::className(), ['idCategorias' => 'id']);
    }

    /**
     * Gets query for [[IdPreguntas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPreguntas()
    {
        return $this->hasMany(Preguntas::className(), ['id' => 'idPreguntas'])->viaTable('preguntascatego', ['idCategorias' => 'id']);
    }

    /**
     * Gets query for [[Tests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTests()
    {
        return $this->hasMany(Tests::className(), ['categoria' => 'id']);
    }
}
