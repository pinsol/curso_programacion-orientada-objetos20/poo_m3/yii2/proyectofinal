<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preguntas".
 *
 * @property int $id
 * @property string|null $texto
 * @property int|null $test_id
 * @property string|null $respuestaCorrecta
 * @property int|null $foto
 *
 * @property Fotos $foto0
 * @property Preguntascatego[] $preguntascategos
 * @property Categorias[] $idCategorias
 * @property Preguntastest[] $preguntastests
 * @property Respuestas[] $respuestas
 */
class Preguntas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'preguntas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_id', 'foto'], 'integer'],
            [['texto'], 'string', 'max' => 255],
            [['respuestaCorrecta'], 'string', 'max' => 1],
            [['foto'], 'exist', 'skipOnError' => true, 'targetClass' => Fotos::className(), 'targetAttribute' => ['foto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
            'test_id' => 'Test ID',
            'respuestaCorrecta' => 'Respuesta Correcta',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Foto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFoto0()
    {
        return $this->hasOne(Fotos::className(), ['id' => 'foto']);
    }

    /**
     * Gets query for [[Preguntascategos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntascategos()
    {
        return $this->hasMany(Preguntascatego::className(), ['idPreguntas' => 'id']);
    }

    /**
     * Gets query for [[IdCategorias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategorias()
    {
        return $this->hasMany(Categorias::className(), ['id' => 'idCategorias'])->viaTable('preguntascatego', ['idPreguntas' => 'id']);
    }

    /**
     * Gets query for [[Preguntastests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntastests()
    {
        return $this->hasMany(Preguntastest::className(), ['pregunta_id' => 'id']);
    }

    /**
     * Gets query for [[Respuestas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRespuestas()
    {
        return $this->hasMany(Respuestas::className(), ['idPregunta' => 'id']);
    }
}
