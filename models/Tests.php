<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tests".
 *
 * @property int $id
 * @property string|null $texto
 * @property string|null $materia
 * @property int|null $categoria
 * @property string|null $fecha
 * @property string|null $titulo
 * @property string|null $tituloImpreso
 *
 * @property Preguntastest[] $preguntastests
 * @property Categorias $categoria0
 */
class Tests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tests';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria'], 'integer'],
            [['fecha'], 'safe'],
            [['texto', 'materia', 'titulo', 'tituloImpreso'], 'string', 'max' => 255],
            [['categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['categoria' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
            'materia' => 'Materia',
            'categoria' => 'Categoria',
            'fecha' => 'Fecha',
            'titulo' => 'Titulo',
            'tituloImpreso' => 'Titulo Impreso',
        ];
    }

    /**
     * Gets query for [[Preguntastests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreguntastests()
    {
        return $this->hasMany(Preguntastest::className(), ['test_id' => 'id']);
    }

    /**
     * Gets query for [[Categoria0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria0()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'categoria']);
    }
}
