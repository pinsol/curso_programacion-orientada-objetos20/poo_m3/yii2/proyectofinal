<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preguntascatego".
 *
 * @property int $id
 * @property int|null $idPreguntas
 * @property int|null $idCategorias
 *
 * @property Categorias $idCategorias0
 * @property Preguntas $idPreguntas0
 */
class Preguntascatego extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'preguntascatego';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPreguntas', 'idCategorias'], 'integer'],
            [['idPreguntas', 'idCategorias'], 'unique', 'targetAttribute' => ['idPreguntas', 'idCategorias']],
            [['idCategorias'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['idCategorias' => 'id']],
            [['idPreguntas'], 'exist', 'skipOnError' => true, 'targetClass' => Preguntas::className(), 'targetAttribute' => ['idPreguntas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPreguntas' => 'Id Preguntas',
            'idCategorias' => 'Id Categorias',
        ];
    }

    /**
     * Gets query for [[IdCategorias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategorias0()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'idCategorias']);
    }

    /**
     * Gets query for [[IdPreguntas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPreguntas0()
    {
        return $this->hasOne(Preguntas::className(), ['id' => 'idPreguntas']);
    }
}
