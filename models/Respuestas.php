<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "respuestas".
 *
 * @property int $id
 * @property string|null $texto
 * @property int|null $idPregunta
 *
 * @property Preguntas $idPregunta0
 */
class Respuestas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'respuestas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPregunta'], 'integer'],
            [['texto'], 'string', 'max' => 255],
            [['idPregunta'], 'exist', 'skipOnError' => true, 'targetClass' => Preguntas::className(), 'targetAttribute' => ['idPregunta' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'texto' => 'Texto',
            'idPregunta' => 'Id Pregunta',
        ];
    }

    /**
     * Gets query for [[IdPregunta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPregunta0()
    {
        return $this->hasOne(Preguntas::className(), ['id' => 'idPregunta']);
    }
}
