﻿/**
                                                  PROYECTO FINAL - GENERADOR DE TESTS
                                                                                                                         **/

/* Creacion Base de Datos */
DROP DATABASE IF EXISTS proyectofinal;
CREATE DATABASE IF NOT EXISTS proyectofinal;

/* Seleccionar la Base de datos con la que vamos a trabajar */
USE proyectofinal;

/* Creacion de las tablas */
-- TABLA PREGUNTAS
CREATE TABLE IF NOT EXISTS preguntas(
	id int AUTO_INCREMENT,
	texto varchar(255),
  test_id int,
	respuestaCorrecta char(1),
  foto int,
	PRIMARY KEY (id)
);

-- TABLA RESPUESTAS
CREATE TABLE IF NOT EXISTS respuestas(
	id int AUTO_INCREMENT,
	texto varchar(255),
	idPregunta int,
	PRIMARY KEY (id)
);

-- TABLA CATEGORIAS
CREATE TABLE IF NOT EXISTS categorias(
	id int AUTO_INCREMENT,
	texto varchar(255),
	PRIMARY KEY (id)

);

-- TABLA FOTOS
CREATE TABLE IF NOT EXISTS fotos(
  id int AUTO_INCREMENT,
  nombre varchar(255),
  PRIMARY KEY (id)
);

-- TABLA TESTS
CREATE TABLE tests (
  id int AUTO_INCREMENT,
  texto  varchar(255),
  materia varchar(255),
  categoria int,
  fecha date,
  titulo varchar(255),
  tituloImpreso varchar(255),
  PRIMARY KEY (id)
);

-- TABLA CATEGORIAS PREGUNTA
CREATE TABLE IF NOT EXISTS preguntasCatego(
	id int NULL AUTO_INCREMENT,
	idPreguntas int,
	idCategorias int,
	PRIMARY KEY (id)
);

-- TABLA PREGUNTAS TEST
CREATE TABLE preguntastest (
  id int AUTO_INCREMENT,
  test_id int,
  pregunta_id int,
  PRIMARY KEY (id)
);

/* Creacion Claves Foráneas */
ALTER TABLE preguntas
  ADD CONSTRAINT FKpreguntasfotos
	FOREIGN KEY (foto) REFERENCES fotos(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE preguntastest
  ADD CONSTRAINT FKPreguntasTestPreguntas
  FOREIGN KEY (pregunta_id) REFERENCES preguntas(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE preguntastest
  ADD CONSTRAINT FKPreguntasTestTests
  FOREIGN KEY (test_id) REFERENCES tests(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE respuestas
  ADD CONSTRAINT FKrespuestaspreguntas
	FOREIGN KEY (idPregunta) REFERENCES preguntas(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE preguntasCatego
  ADD CONSTRAINT FKpreguntasCategoPreguntas
  FOREIGN KEY (idPreguntas) REFERENCES preguntas(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE preguntasCatego
  ADD CONSTRAINT FKpreguntasCategoCategorias
	FOREIGN KEY (idCategorias) REFERENCES categorias(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE tests
  ADD CONSTRAINT FKTestsCategorias
  FOREIGN KEY (categoria) REFERENCES categorias(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

-- CLAVES UNICAS
ALTER TABLE preguntasCatego 
  ADD UNIQUE KEY (idPreguntas, idCategorias);